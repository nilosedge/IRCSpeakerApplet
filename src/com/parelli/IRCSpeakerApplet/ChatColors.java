package com.parelli.IRCSpeakerApplet;

public class ChatColors {	
	public static String[] colors = {
		"#000000", //black
		"#330066", //drk purple
		"#660099", //med purple
		"#000066", //drk blue
		"#0000CC", //med blue
		"#33CCFF", //lgt blue
		"#003300", //drk grn
		"#336600", //med grn
		"#33CC33", //lgt green
		"#993300", //drk orange
		"#CC6600", //med orange
		"#FF6600", //lgt orange
		"#990000", //drk red
		"#FF0000", //med red
		"#FF3399" //lgt red
	};
}
