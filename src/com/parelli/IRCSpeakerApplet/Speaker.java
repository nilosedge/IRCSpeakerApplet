package com.parelli.IRCSpeakerApplet;

import java.util.Locale;
import javax.speech.EngineCreate;
import javax.speech.EngineList;
import javax.speech.synthesis.Synthesizer;
import javax.speech.synthesis.SynthesizerModeDesc;
import com.sun.speech.freetts.jsapi.FreeTTSEngineCentral;

public class Speaker {

    protected Synthesizer synthesizer;
    private boolean isSpeaking = false;
 
    public Speaker(boolean speakchat) {
    	isSpeaking = speakchat;
        createSynthesizer();
    }
    
    public void setisSpeaking(boolean speaking) {
    	isSpeaking = speaking;
    }
    
    public void sayText(String text) {
    	//System.out.println(text);
    	if(isSpeaking) synthesizer.speakPlainText(text, null);
    }

    public void createSynthesizer() {
        try {
            SynthesizerModeDesc desc = new SynthesizerModeDesc(null, "general", Locale.US, Boolean.FALSE, null);
            FreeTTSEngineCentral central = new FreeTTSEngineCentral();
            EngineList list = central.createEngineList(desc);        
            if (list.size() > 0) { 
                EngineCreate creator = (EngineCreate) list.get(0); 
                synthesizer = (Synthesizer) creator.createEngine(); 
            } 
            if (synthesizer == null) {
                System.err.println("Cannot create synthesizer");
                System.exit(1);
            }
            synthesizer.allocate();
            synthesizer.resume();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
