package com.parelli.IRCSpeakerApplet;

import java.awt.Color;

public class Nick {
	private String name;
	private Color color;
	
	public Nick(String name, Color color) {
		this.name = name;
		this.color = color;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
	
}
