package com.parelli.IRCSpeakerApplet;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JToggleButton;
import javax.swing.colorchooser.AbstractColorChooserPanel;

public class ColorChooserPanel2 extends AbstractColorChooserPanel implements ActionListener {

	private static final long serialVersionUID = -4830263191174766137L;
	private JToggleButton jtb = null;

	public void updateChooser() {	
	}

	protected void buildChooser() {
		setLayout(new GridLayout(2, 0)); // 2 is # of rows; 2nd param is ignored
		
		ButtonGroup chooser = new ButtonGroup();
		
		for(int i = 0; i < ChatColors.colors.length; i++) {
			jtb = createColorButton(ChatColors.colors[i], Color.decode(ChatColors.colors[i]));
			jtb.setActionCommand(ChatColors.colors[i]);
			chooser.add(jtb);
			add(jtb);
		}
	}

	public String getDisplayName() {
		return "Colors";
	}

	public Icon getSmallDisplayIcon() {
		return null;
	}

	public Icon getLargeDisplayIcon() {
		return null;
	}

	public void actionPerformed(ActionEvent e) {
		Color c = null;
		JToggleButton jtb = (JToggleButton)e.getSource();
		String cmd = jtb.getActionCommand();
		
		for(int i = 0; i < ChatColors.colors.length; i++) {
			if(cmd.equals(ChatColors.colors[i])) {
				c = Color.decode(ChatColors.colors[i]);
			}
		}	
		getColorSelectionModel().setSelectedColor(c);

	}
	
	protected JToggleButton createColorButton(String name, Color color) {
		JToggleButton b = new JToggleButton();
		b.setActionCommand(name);
		b.addActionListener(this);
		
		b.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		b.setPreferredSize(new Dimension(15,15));
		b.setBackground(color);
		b.setForeground(color);
		b.setOpaque(true);
		
		return b;
	}

}
