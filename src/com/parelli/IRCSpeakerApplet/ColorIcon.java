package com.parelli.IRCSpeakerApplet;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;

public class ColorIcon implements Icon {
	 private Color fillColor;
     private int w, h;

     public ColorIcon(Color fillColor, int w, int h) {
             this.fillColor = fillColor;
             this.w = w;
             this.h = h;
     }
     public void paintIcon(Component c, Graphics g, int x, int y) {
             g.setColor(Color.black);
             g.drawRect(x, y, w-1, h-1);
             g.setColor(fillColor);
             g.fillRect(x+1, y+1, w-2, h-2);
     }
     public int getIconWidth() {
             return w;
     }
     public int getIconHeight() {
             return h;
     }
}
