package com.parelli.IRCSpeakerApplet;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JPanel;
import javax.swing.JApplet;

import org.schwering.irc.lib.IRCConnection;

public class IRCSpeakerApplet extends JApplet {

	private static final long serialVersionUID = 3813048127999101353L;
	public final static String IRC_SERVER = "www.parellisavvyclub.com";
	public final static int IRC_PORT_LOW = 6667; 
	public final static int IRC_PORT_HIGH = 6669; 
	public final static String IRC_CHANNEL = "#test";
	private Speaker speaker = null;
	private boolean speaking = true;
	
	// IRC Connection
	private IRCConnection conn;
	
	// target can be a channel or nickname
	private String target;
	
	// my nickname
	private Nick nickName;
	
	// user's browser type
	private String browser;

	private ChatUI chatUI = null;
	/**
	 * This is the default constructor
	 */
	public IRCSpeakerApplet() {
		speaker = new Speaker(speaking);
		// do what?
	}

	/**
	 * This method initializes this
	 * 
	 * @return void
	 */
	public void init() {
		// set from applet params
		nickName = new Nick(getParameter("nick"), Color.BLUE);
		target = IRC_CHANNEL;
		browser = getParameter("browser");
		
		this.setSize(300, 200);
		this.setContentPane(getJContentPane());		
	}

	/**
	 * This method initializes jContentPane
	 * 
	 * @return javax.swing.JPanel
	 */
	private JPanel getJContentPane() {
		if(chatUI == null) {
			chatUI = new ChatUI(target, nickName, browser, speaker);
		}
		return chatUI;
	}

	private void connect() throws IOException {
		conn = new IRCConnection(
				IRC_SERVER, 
				IRC_PORT_LOW, 
				IRC_PORT_HIGH, 
				null, 
				nickName.getName(), 
				nickName.getName(), 
				nickName.getName());
		conn.addIRCEventListener(new ChatListener(chatUI));
		conn.setPong(false);
		conn.setDaemon(false);
		conn.setColors(true);
		conn.connect();
	}
	
	public void start() {
		target = IRC_CHANNEL;
		try {
			connect();
		} catch (IOException e) {
			System.out.println("Unable to connect to chat server");
			e.printStackTrace();
		}
		conn.doJoin(target);
		
		//set the connection to the UI
		chatUI.setIRCConnection(conn);
	}
	
	public void stop() {
		conn.doQuit();
		conn.close();
	}
}
