package com.parelli.IRCSpeakerApplet;

import java.awt.Color;
import java.io.IOException;

import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

public class ScrollableColoredChatPanel extends JScrollPane {

	private static final long serialVersionUID = -6311129548281378355L;
	private JEditorPane chatPane = null;
	private HTMLDocument doc;
	private HTMLEditorKit editorKit;
	private int FontSize = 12;
	private boolean isChatScrollEnabled = true;
	
	public ScrollableColoredChatPanel() {
		super();
		initialize();
	}

	private void initialize() {	
		this.setSize(300, 200);
		this.setViewportView(getChatPane());
	}

	private JEditorPane getChatPane() {
		if (chatPane == null) {
			chatPane = new JEditorPane();
			chatPane.setEditable(false);
			chatPane.setContentType("text/html");
			editorKit = (HTMLEditorKit)chatPane.getEditorKit();
			doc = (HTMLDocument)chatPane.getDocument();
		}
		return chatPane;
	}
	
	public void appendLine(String line, Color color) {		
		//replace the < and > chars with html tags
		line = line.replaceFirst("<", "&lt;");
		line = line.replaceFirst(">", "&gt;");
		line = "<font style=\"font-size: " + FontSize + "pt;\" face='veranda' color='" + colorToHexString(color) + "'>" + line + "</font>";
		//System.out.println(line);
		try {
			//System.out.println(line);
			editorKit.insertHTML(doc, doc.getLength(), line, 0, 0, null);
			//doc.insertString(doc.getLength(), line + NL, modelAttr);
		} catch (BadLocationException e) {
			System.out.println("Unable to add message to chat pane: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Unable to add message to chat pane: ");
			e.printStackTrace();
		}
		if(isChatScrollEnabled) {
			chatPane.setCaretPosition(doc.getLength());		
		}		
	}
	
	public void setFontSize(int size) {
		FontSize = size;
	}
	
	public void clear() {
		try {
			System.out.println("called clear");
			doc.remove(0, doc.getLength());
		} catch (BadLocationException e) {
			System.out.println("Unable to clear chat pane: ");
			e.printStackTrace();
		}
	}
	
	private static String colorToHexString(Color c) {
		if(c == null) {
			System.out.println("colorToHexString got a null Color!");
		}
	    return Integer.toHexString(0xFF000000 | c.getRGB()).substring(2);
	}

	public boolean isChatScrollEnabled() {
		return isChatScrollEnabled;
	}

	public void setChatScrollEnabled(boolean isChatScrollEnabled) {
		this.isChatScrollEnabled = isChatScrollEnabled;
	}

}
