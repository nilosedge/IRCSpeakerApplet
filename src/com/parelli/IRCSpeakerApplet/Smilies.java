package com.parelli.IRCSpeakerApplet;

public class Smilies {

	private String sm[] = new String[32];
	private String lk[] = new String[32];

	public Smilies() {

		sm[0] = ":)";
		sm[1] = ":-)";
		sm[2] = ":D";
		sm[3] = ":-D";
		sm[4] = ":(";
		sm[5] = ":mrgreen:";
		sm[6] = ":-o";
		sm[7] = ":shock:";
		sm[8] = ":?:";
		sm[9] = "8)";
		sm[10] = ":lol:";
		sm[11] = ":x";
		sm[12] = ":P";
		sm[13] = ":-P";
		sm[14] = ":oops:";
		sm[15] = ":cry:";
		sm[16] = ":evil:";
		sm[17] = ":twisted:";
		sm[18] = ":roll:";
		sm[19] = ":wink:";
		sm[20] = ";)";
		sm[21] = ";-)";
		sm[22] = ":!:";
		sm[23] = ":?";
		sm[24] = ":idea:";
		sm[25] = ":arrow:";
		sm[26] = ":-(";
		sm[27] = ":hunf:";
		sm[28] = ":XD:";
		sm[29] = ":thumbup:";
		sm[30] = ":thumbdown:";
		sm[31] = ":|";

		lk[0] = "3b63d1616c5dfcf29f8a7a031aaa7cad.gif";
		lk[1] = "3b63d1616c5dfcf29f8a7a031aaa7cad.gif";
		lk[2] = "283a16da79f3aa23fe1025c96295f04f.gif";
		lk[3] = "283a16da79f3aa23fe1025c96295f04f.gif";
		lk[4] = "9d71f0541cff0a302a0309c5079e8dee.gif";
		lk[5] = "ed515dbff23a0ee3241dcc0a601c9ed6.gif";
		lk[6] = "47941865eb7bbc2a777305b46cc059a2.gif";
		lk[7] = "385970365b8ed7503b4294502a458efa.gif";
		lk[8] = "0a4d7238daa496a758252d0a2b1a1384.gif";
		lk[9] = "b2eb59423fbf5fa39342041237025880.gif";
		lk[10] = "97ada74b88049a6d50a6ed40898a03d7.gif";
		lk[11] = "1069449046bcd664c21db15b1dfedaee.gif";
		lk[12] = "69934afc394145350659cd7add244ca9.gif";
		lk[13] = "69934afc394145350659cd7add244ca9.gif";
		lk[14] = "499fd50bc713bfcdf2ab5a23c00c2d62.gif";
		lk[15] = "c30b4198e0907b23b8246bdd52aa1c3c.gif";
		lk[16] = "2e207fad049d4d292f60607f80f05768.gif";
		lk[17] = "908627bbe5e9f6a080977db8c365caff.gif";
		lk[18] = "2786c5c8e1a8be796fb2f726cca5a0fe.gif";
		lk[19] = "8a80c6485cd926be453217d59a84a888.gif";
		lk[20] = "8a80c6485cd926be453217d59a84a888.gif";
		lk[21] = "8a80c6485cd926be453217d59a84a888.gif";
		lk[22] = "9293feeb0183c67ea1ea8c52f0dbaf8c.gif";
		lk[23] = "136dd33cba83140c7ce38db096d05aed.gif";
		lk[24] = "8f7fb9dd46fb8ef86f81154a4feaada9.gif";
		lk[25] = "d6741711aa045b812616853b5507fd2a.gif";
		lk[26] = "9d71f0541cff0a302a0309c5079e8dee.gif";
		lk[27] = "0320a00cb4bb5629ab9fc2bc1fcc4e9e.gif";
		lk[28] = "49869fe8223507d7223db3451e5321aa.gif";
		lk[29] = "e8a506dc4ad763aca51bec4ca7dc8560.gif";
		lk[30] = "e78feac27fa924c4d0ad6cf5819f3554.gif";
		lk[31] = "1cfd6e2a9a2c0cf8e74b49b35e2e46c7.gif";
	
	}

	public String makeSmile(String in) {
		for(int i = 0; i < sm.length; i++) {
			//in = in.replace(sm[i], "<img src=\"http://www.parellisavvyclub.com/forum/images/smilies/" +lk[i] + "\">");
			in = replace(in, sm[i], "<img src=\"http://www.parellisavvyclub.com/forum/images/smilies/" +lk[i] + "\">");
		}
		return in;
	}
	
	public String[] getSmiliesText() {
		return sm;
	}
	
	public String[] getSmiliesFiles() {
		return lk;
	}
	
	private String replace(String source, String find, String replace) {
        StringBuffer result = new StringBuffer(source);
        int position = source.indexOf(find);
        while (position > -1) {
            result.replace(position, position + find.length(), replace);
            position = result.toString().indexOf(find,
                    position + replace.length());
        }
        return result.toString();
    }
}
