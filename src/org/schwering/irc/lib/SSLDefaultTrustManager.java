/* 
 * IRClib -- A Java Internet Relay Chat library -- class SSLDefaultTrustManager
 * Copyright (C) 2002, 2003 Christoph Schwering <ch@schwering.org>
 * 
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation; either version 2.1 of the License, or (at your option) 
 * any later version.
 * This library is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more 
 * details.
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, write to the Free Software Foundation, 
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA 
 */
 
package org.schwering.irc.lib;

import com.sun.net.ssl.X509TrustManager;
import java.security.cert.X509Certificate;

/**
 * The default <code>TrustManager</code> of the 
 * <code>SSLIRCConnection</code>.
 * <p>
 * It automatically accepts the X509 certificate.
 * <p>
 * In many cases you should change the <code>SSLIRCConnection</code>'s 
 * <code>TrustManager</code>. For examle if you write an IRC client for human
 * users, you may want to ask the user whether he accepts the server's 
 * certificate or not. You could do this by a new class which extends the
 * <code>SSLDefaultTrustManager</code> class and overrides the 
 * <code>checkServerTrusted</code> method and asks the user whether he wants to
 * accept the certification or not.
 * @author Christoph Schwering &lt;ch@schwering.org&gt;
 * @version 1.13
 * @see SSLIRCConnection
 * @see com.sun.net.ssl.TrustManager
 */
public class SSLDefaultTrustManager implements X509TrustManager {

  /**
   * The <code>X509Certificate</code>s which are accepted.
   */
  protected X509Certificate[] accepted = new X509Certificate[0];

// ------------------------------

  /**
   * Creates a new instance of the <code>SSLDefaultTrustManager</code> class.
   */
  public SSLDefaultTrustManager() {
    // nothing
  }

// ------------------------------

  /**
   * Does nothing. This method would check whether we (the server) trust the 
   * client. But we are the client and not the server. <br />
   * It's final so that nobody can override it; it would make no sense.
   * @param chain The peer certificate chain.
   * @return Always <code>false</code>.
   */
  public final boolean isClientTrusted(X509Certificate chain[]) {
    return false;
  }

// ------------------------------

  /**
   * Invoked when the client should check whether he trusts the server or not.
   * This method trusts the server. But this method can be overriden and then
   * ask the user whether he truts the client or not.
   * @param chain The peer certificate chain.
   * @return Always <code>true</code>.
   */
  public boolean isServerTrusted(X509Certificate chain[]) {
    accepted = chain;
    return true;
  }

// ------------------------------

  /**
   * Returns the accepted certificates. They are set in the 
   * <code>checkServerTrusted</code> method.
   * @return A non-null (possibly empty) array of acceptable CA issuer 
   *         certificates.
   */
  public X509Certificate[] getAcceptedIssuers() {
    return accepted;
  }

// ------------------------------

}